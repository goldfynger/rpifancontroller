#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include "stm32f0xx_hal.h"
#include "tim.h"
#include "i2c.h"
#include "main.h"
#include "rpi_fan_controller.h"


#define RPI_FAN_CONTROLLER_ZERO_PULSE_WIDTH     ((uint8_t)0)
#define RPI_FAN_CONTROLLER_MIN_PULSE_WIDTH      ((uint8_t)40)   /* If new width is less than min, zero width is applied. */
#define RPI_FAN_CONTROLLER_START_PULSE_WIDTH    ((uint8_t)55)   /* If current width is zero and new width is less than start, start width is applied for 0.5 seconds, then applied new width. */
#define RPI_FAN_CONTROLLER_MAX_PULSE_WIDTH      ((uint8_t)100)  /* If new width is large than max, max width is applied. */

#define RPI_FAN_CONTROLLER_UNAVAILABLE_REG_VAL  ((uint8_t)0)    /* Value of all unavailable registers. */
#define RPI_FAN_CONTROLLER_DUMMY_TRANSMIT_VAL   ((uint8_t)0xFF) /* Value of dummy transmits when master want read more data. */


typedef enum
{
    RPI_FAN_CONTROLLER_REGISTER_PULSE_WIDTH     = ((uint8_t)0xA0), /* Current PWM pulse width. */
}
RPI_FAN_CONTROLLER_RegisterMapTypeDef;

typedef enum
{
    RPI_FAN_CONTROLLER_STATE_INIT               = ((uint8_t)0x00),
    RPI_FAN_CONTROLLER_STATE_WAIT_FOR_START     = ((uint8_t)0x01),
    RPI_FAN_CONTROLLER_STATE_WAIT_FOR_REGISTER  = ((uint8_t)0x02),
    RPI_FAN_CONTROLLER_STATE_WAIT_FOR_VALUE     = ((uint8_t)0x03),
    RPI_FAN_CONTROLLER_STATE_WAIT_FOR_COMPLETE  = ((uint8_t)0x04),
    RPI_FAN_CONTROLLER_STATE_COMPLETED          = ((uint8_t)0x05),
}
RPI_FAN_CONTROLLER_StateTypeDef;


static volatile uint8_t _pulseWidth = RPI_FAN_CONTROLLER_ZERO_PULSE_WIDTH;
static volatile bool _isStartTimerRun = false;
static volatile RPI_FAN_CONTROLLER_RegisterMapTypeDef _selectedRegister = RPI_FAN_CONTROLLER_REGISTER_PULSE_WIDTH;
static volatile RPI_FAN_CONTROLLER_StateTypeDef _state = RPI_FAN_CONTROLLER_STATE_INIT;
uint8_t _buffer = 0;


static void RPI_FAN_CONTROLLER_StartPwm(void);
static void RPI_FAN_CONTROLLER_NormalizeAndSetPwmPulseWidth(uint8_t pulseWidth);


__NO_RETURN void RPI_FAN_CONTROLLER_Process(void)
{
    RPI_FAN_CONTROLLER_StartPwm();
    RPI_FAN_CONTROLLER_NormalizeAndSetPwmPulseWidth(RPI_FAN_CONTROLLER_ZERO_PULSE_WIDTH);
    
    __HAL_TIM_SET_COUNTER(&htim16, 0);
    __HAL_TIM_CLEAR_FLAG(&htim16, TIM_SR_UIF); /* Interrupt flag is set in first start, so clear it. */
    if (HAL_TIM_Base_Start_IT(&htim16) != HAL_OK)
    {
        Error_Handler();
    }
        
    while (true)
    {
        _state = RPI_FAN_CONTROLLER_STATE_WAIT_FOR_START;
        
        if(HAL_I2C_EnableListen_IT(&hi2c1) != HAL_OK)
        {
            Error_Handler();
        }
        
        while (_state != RPI_FAN_CONTROLLER_STATE_COMPLETED) /* Wait for complete. */
        {
        }
        
        _state = RPI_FAN_CONTROLLER_STATE_INIT;
        _buffer = 0;
    }
}


static void RPI_FAN_CONTROLLER_StartPwm(void)
{
    if (HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1) != HAL_OK)
    {
        Error_Handler();
    }
}

static void RPI_FAN_CONTROLLER_NormalizeAndSetPwmPulseWidth(uint8_t pulseWidth)
{
    if (!_isStartTimerRun)
    {
        uint8_t pulseWidthToStore = 0;
        uint8_t pulseWidthToPwm = 0;
        
        if (pulseWidth < RPI_FAN_CONTROLLER_MIN_PULSE_WIDTH) /* Stop PWM. */
        {
            pulseWidthToStore = RPI_FAN_CONTROLLER_ZERO_PULSE_WIDTH;
            pulseWidthToPwm = RPI_FAN_CONTROLLER_ZERO_PULSE_WIDTH;
        }
        else if (_pulseWidth == RPI_FAN_CONTROLLER_ZERO_PULSE_WIDTH && pulseWidth < RPI_FAN_CONTROLLER_START_PULSE_WIDTH) /* Run PWM start sequence with start timer. */
        {
            pulseWidthToStore = pulseWidth;
            pulseWidthToPwm = RPI_FAN_CONTROLLER_START_PULSE_WIDTH;
            
            __HAL_TIM_SET_COUNTER(&htim3, 0);
            __HAL_TIM_CLEAR_FLAG(&htim3, TIM_SR_UIF); /* Interrupt flag is set in first start, so clear it. */
            if (HAL_TIM_Base_Start_IT(&htim3) != HAL_OK)
            {
                Error_Handler();
            }
            _isStartTimerRun = true;
        }
        else if (pulseWidth > RPI_FAN_CONTROLLER_MAX_PULSE_WIDTH) /* Run PWM at max pulse width. */
        {
            pulseWidthToStore = RPI_FAN_CONTROLLER_MAX_PULSE_WIDTH;
            pulseWidthToPwm = RPI_FAN_CONTROLLER_MAX_PULSE_WIDTH;
        }
        else
        {
            pulseWidthToStore = pulseWidth;
            pulseWidthToPwm = pulseWidth;
        }
        
        _pulseWidth = pulseWidthToStore;
        
        if (pulseWidthToPwm > htim14.Init.Period)
        {
            pulseWidthToPwm = htim14.Init.Period;
        }
        
        __HAL_TIM_SET_COMPARE(&htim14, TIM_CHANNEL_1, pulseWidthToPwm);
    }
    else /* _isStartTimerRun == true */
    {
        if (pulseWidth < RPI_FAN_CONTROLLER_MIN_PULSE_WIDTH) /* Stop PWM. */
        {
            _pulseWidth = RPI_FAN_CONTROLLER_ZERO_PULSE_WIDTH;
        }
        else if (pulseWidth > RPI_FAN_CONTROLLER_MAX_PULSE_WIDTH) /* Run PWM at max pulse width. */
        {
            _pulseWidth = RPI_FAN_CONTROLLER_MAX_PULSE_WIDTH;
        }
        else
        {
            _pulseWidth = pulseWidth;
        }
    }
}


/* Timer ITR priority MUST be equal to I2C ITR priority. So, both ITR can not interrupt each other. */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if (htim == &htim3 && _isStartTimerRun) /* Timer used as end on start sequence - 0.5 second after run. */
    {
        uint8_t pulseWidth = _pulseWidth;
        
        if (pulseWidth > htim14.Init.Period)
        {
            pulseWidth = htim14.Init.Period;
        }
        
        __HAL_TIM_SET_COMPARE(&htim14, TIM_CHANNEL_1, pulseWidth);
        
        _isStartTimerRun = false;
        if (HAL_TIM_Base_Stop_IT(&htim3) != HAL_OK) /* Stop timer here. */
        {
            Error_Handler();
        }
    }
    else if (htim == &htim16 && !_isStartTimerRun) /* Timer used to activate start sequence for time to time in case of fan is stopped - each 300 seconds. */
    {
        if (_pulseWidth >= RPI_FAN_CONTROLLER_MIN_PULSE_WIDTH && _pulseWidth <= RPI_FAN_CONTROLLER_START_PULSE_WIDTH)
        {
            __HAL_TIM_SET_COUNTER(&htim3, 0);
            __HAL_TIM_CLEAR_FLAG(&htim3, TIM_SR_UIF); /* Interrupt flag is set in first start, so clear it. */
            if (HAL_TIM_Base_Start_IT(&htim3) != HAL_OK)
            {
                Error_Handler();
            }
            _isStartTimerRun = true;
            
            uint8_t pulseWidth = RPI_FAN_CONTROLLER_START_PULSE_WIDTH;
            
            if (pulseWidth > htim14.Init.Period)
            {
                pulseWidth = htim14.Init.Period;
            }
            
            __HAL_TIM_SET_COMPARE(&htim14, TIM_CHANNEL_1, pulseWidth);
        }
    }
}

void HAL_I2C_SlaveTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    _buffer = RPI_FAN_CONTROLLER_DUMMY_TRANSMIT_VAL;
    
    if(HAL_I2C_Slave_Seq_Transmit_IT(&hi2c1, &_buffer, 1, I2C_NEXT_FRAME) != HAL_OK) /* Dummy transmit in case of master wants transmit more data. */
    {
        Error_Handler();
    }
}

void HAL_I2C_SlaveRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    if (hi2c == &hi2c1)
    {
        if (_state == RPI_FAN_CONTROLLER_STATE_WAIT_FOR_REGISTER)
        {
            _selectedRegister = (RPI_FAN_CONTROLLER_RegisterMapTypeDef)_buffer;
            _state = RPI_FAN_CONTROLLER_STATE_WAIT_FOR_VALUE;       /* Register selected, wait for register read or write. */
            
            if(HAL_I2C_Slave_Seq_Receive_IT(&hi2c1, &_buffer, 1, I2C_FIRST_FRAME) != HAL_OK) /* Receive write register. */
            {
                Error_Handler();
            }
        }
        else if (_state == RPI_FAN_CONTROLLER_STATE_WAIT_FOR_VALUE)
        {
            _state = RPI_FAN_CONTROLLER_STATE_WAIT_FOR_COMPLETE;    /* Process selected register value. */
            
            switch (_selectedRegister)
            {
                case RPI_FAN_CONTROLLER_REGISTER_PULSE_WIDTH:
                {
                    RPI_FAN_CONTROLLER_NormalizeAndSetPwmPulseWidth(_buffer);
                }
                break;
                
                default:
                {
                }
                break;
            }
            __HAL_I2C_GENERATE_NACK(hi2c); /* NACK in case of master wants transmit more data. */
        }
        else
        {
            __HAL_I2C_GENERATE_NACK(hi2c);
        }
    }
}

void HAL_I2C_AddrCallback(I2C_HandleTypeDef *hi2c, uint8_t TransferDirection, uint16_t AddrMatchCode)
{
    if (hi2c == &hi2c1)
    {
        if (AddrMatchCode == hi2c1.Init.OwnAddress1)
        {
            if(TransferDirection == I2C_DIRECTION_TRANSMIT)
            {
                _state = RPI_FAN_CONTROLLER_STATE_WAIT_FOR_REGISTER;    /* Master TX initiated, wait for register. */
                
                if(HAL_I2C_Slave_Seq_Receive_IT(&hi2c1, &_buffer, 1, I2C_FIRST_FRAME) != HAL_OK)
                {
                    Error_Handler();
                }
            }
            else /* TransferDirection == I2C_DIRECTION_RECEIVE */
            {
                _state = RPI_FAN_CONTROLLER_STATE_WAIT_FOR_COMPLETE;    /* Slave TX, send selected register value. */
                
                switch (_selectedRegister)
                {
                    case RPI_FAN_CONTROLLER_REGISTER_PULSE_WIDTH:
                    {
                        _buffer = _pulseWidth;
                    }
                    break;
                    
                    default:
                    {
                        _buffer = RPI_FAN_CONTROLLER_UNAVAILABLE_REG_VAL;
                    }
                    break;
                }                

                if(HAL_I2C_Slave_Seq_Transmit_IT(&hi2c1, &_buffer, 1, I2C_NEXT_FRAME) != HAL_OK)
                {
                    Error_Handler();
                }
            }
        }
        else
        {
            Error_Handler();
        }
    }
}

void HAL_I2C_ListenCpltCallback(I2C_HandleTypeDef *hi2c)
{
    _state = RPI_FAN_CONTROLLER_STATE_COMPLETED;
}

void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hi2c)
{
    if (HAL_I2C_GetError(hi2c) != HAL_I2C_ERROR_AF)
    {    
        Error_Handler();
    }
}
