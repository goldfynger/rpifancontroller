#ifndef __RPI_FAN_CONTROLLER_H
#define __RPI_FAN_CONTROLLER_H


#include "stm32f0xx_hal.h"


__NO_RETURN void RPI_FAN_CONTROLLER_Process(void);


#endif /* __RPI_FAN_CONTROLLER_H */
