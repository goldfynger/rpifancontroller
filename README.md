## RPi fan controller

Firmware for RPi 3 fan controller based on STM32F030F4 MCU. Receives fan pulse width through I2C from RPi, controls fan using PWM.

[RPi temperature controller service](https://bitbucket.org/goldfynger/rpiutils)

[Hardware](https://easyeda.com/goldfynger/RPiFanController-75ff7e90a4534b398fe02e07fc22162f)